# syntax=docker/dockerfile:1
FROM ubuntu:20.04 AS base

#Define ARG instruction
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    autoconf \
    automake \
    bash-completion \
    build-essential \
    ccache \
    cmake \
    curl \
    doxygen \
    g++ \
    gcc \
    gfortran \
    git \
    git-core \
    git-lfs \
    graphviz \
    ipython3 \
    iputils-ping \
    iproute2 \
    libblas-dev \
    libblas3 \
    libboost-all-dev \
    libboost-dev \
    libbz2-dev \
    libffi-dev \
    libgdbm-compat-dev \
    liblapack-dev \
    liblapack3 \
    liblz4-dev \
    liblzma-dev \
    libncurses5-dev \
    libncursesw5-dev \
    libprotobuf-dev \
    libreadline-dev \
    libspdlog-dev \
    libsqlite3-dev \
    libssl-dev \
    libtool \
    libzstd-dev \
    llvm \
    pandoc \
    pandoc-citeproc \
    pkg-config \
    protobuf-compiler \
    python-openssl \
    python3-dev \
    python3-matplotlib \
    python3-pip \
    python3-venv \
    snapd \
    sudo \
    swig \
    tcpdump \
    tk-dev \
    unzip \
    valgrind \
    vim \
    wget \
    wget \
    xclip \
    xz-utils \
    zlib1g-dev \
    --install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && echo "source /usr/share/bash-completion/completions/git" >> ~/.bashrc

RUN pip3 install --upgrade pip && \
    pip3 install control && \
    pip3 install mkdocs && \
    pip3 install pymdown-extensions && \
    pip3 install pygments && \
    pip3 install numpy==1.24.4 && \
    pip3 install scipy==1.10.1

RUN wget https://apt.llvm.org/llvm-snapshot.gpg.key \
    && apt-key add llvm-snapshot.gpg.key \
    && apt update \
    && apt install -y software-properties-common \
    && add-apt-repository 'deb http://apt.llvm.org/focal/ llvm-toolchain-focal-13 main' \
    && apt update \
    && apt install -y clang-format-13 \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /home/mpmca-user
WORKDIR /home/mpmca-user

RUN apt autoremove
FROM base AS eigen
#Install eigen
ADD --keep-git-dir=false https://gitlab.com/libeigen/eigen.git#3.3.7 /home/mpmca-user/eigen
RUN cd eigen && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make -j1 install

#Install Googletest
FROM base AS googletest
ADD --keep-git-dir=false https://github.com/google/googletest.git#v1.10.x /home/mpmca-user/googletest/
RUN cd googletest && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make -j1 && make install

#Install Benchmark
FROM googletest AS benchmark
ADD --keep-git-dir=false https://github.com/google/benchmark.git#v1.5.0 /home/mpmca-user/benchmark/
RUN cd benchmark && cp -r ../googletest .
RUN cd benchmark && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make -j1 && make install

#Install Blaze
FROM base AS blaze
ADD --keep-git-dir=false https://bitbucket.org/blaze-lib/blaze.git#v3.6 /home/mpmca-user/blaze
RUN cd blaze && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build . && make install

#Install json
FROM base AS json
ADD --keep-git-dir=false https://github.com/nlohmann/json.git#v3.7.3 /home/mpmca-user/json
RUN cd json/ && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF .. && make install

#Install casadi
FROM base AS casadi
ADD --keep-git-dir=false https://github.com/casadi/casadi.git#3.4.3 /home/mpmca-user/casadi
ADD --keep-git-dir=false https://gitlab.com/predictive-cueing/casadi-patch.git /home/mpmca-user/casadi_patch
RUN cp casadi_patch/mem.h casadi/casadi/ && cp casadi_patch/code_generator.cpp casadi/casadi/core/ && cp casadi_patch/casadi.i casadi/swig/casadi.i
RUN cd casadi && mkdir build && cd build && cmake -DWITH_PYTHON=ON -DWITH_PYTHON3=ON -DWITH_COMMON=ON -DCMAKE_BUILD_TYPE=Release .. && cmake --build . && cmake --install . --prefix /opt/casadi

#Install Blasfeo
FROM base AS blasfeo
ADD --keep-git-dir=false https://github.com/giaf/blasfeo.git#9c9e07923de801eab59937316888d20f5caa4f48 /home/mpmca-user/blasfeo
RUN sed -i '52s/.*/TARGET = X64_INTEL_HASWELL/' /home/mpmca-user/blasfeo/Makefile.rule && sed -i '53s/.*/# TARGET = X64_INTEL_SANDY_BRIDGE/' /home/mpmca-user/blasfeo/Makefile.rule
RUN cd blasfeo && mkdir -p lib && make && make install_static

#Install hpmpc
FROM blasfeo AS hpmpc
ADD --keep-git-dir=false https://github.com/giaf/hpmpc.git#35f36c98b7707aad7fbcb105d1a79fa5e6a0bd4c /home/mpmca-user/hpmpc
RUN cd hpmpc && make && make install_static

#Install Matplotplusplus
FROM base AS matplotplusplus
ADD --keep-git-dir=false https://github.com/alandefreitas/matplotplusplus.git /home/mpmca-user/matplotplusplus
RUN cd matplotplusplus && mkdir build && cd build && cmake .. -DMATPLOTPP_BUILD_EXAMPLES=OFF && make -j1 && make install

FROM base AS tmpc
COPY --from=eigen /usr/local/include/eigen3 /usr/local/include/eigen3
COPY --from=eigen /usr/local/share/pkgconfig/eigen3* /usr/local/share/pkgconfig/
COPY --from=eigen /usr/local/share/eigen3 /usr/local/share/eigen3

COPY --from=googletest /usr/local/include/gmock /usr/local/include/gmock
COPY --from=googletest /usr/local/include/gtest /usr/local/include/gtest
COPY --from=googletest /usr/local/lib/libgmock* /usr/local/lib/
COPY --from=googletest /usr/local/lib/libgtest* /usr/local/lib/
COPY --from=googletest /usr/local/lib/pkgconfig/gmock* /usr/local/lib/pkgconfig/
COPY --from=googletest /usr/local/lib/pkgconfig/gtest* /usr/local/lib/pkgconfig/
COPY --from=googletest /usr/local/lib/cmake/GTest /usr/local/lib/cmake/GTest

COPY --from=benchmark /usr/local/lib/libbench* /usr/local/lib/
COPY --from=benchmark /usr/local/include/benchmark /usr/local/include/benchmark
COPY --from=benchmark /usr/local/lib/cmake/benchmark /usr/local/lib/cmake/benchmark
COPY --from=benchmark /usr/local/lib/pkgconfig/benchmark* /usr/local/lib/pkgconfig/

COPY --from=blaze /usr/local/include/blaze /usr/local/include/blaze
COPY --from=blaze /usr/local/share/blaze /usr/local/share/blaze
COPY --from=json /usr/local/include/nlohmann /usr/local/include/nlohmann
COPY --from=json /usr/local/lib/cmake/nlohmann_json /usr/local/lib/cmake/nlohmann_json
COPY --from=casadi /usr/local/lib/cmake/casadi /usr/local/lib/cmake/casadi
COPY --from=casadi /usr/local/include/casadi /usr/local/include/casadi
COPY --from=casadi /usr/lib/python3/dist-packages/casadi /usr/lib/python3/dist-packages/casadi
COPY --from=casadi /usr/local/lib/libcasadi* /usr/local/lib/
COPY --from=blasfeo /opt/blasfeo /opt/blasfeo
COPY --from=hpmpc /opt/hpmpc /opt/hpmpc


#Install TMPC
ADD --keep-git-dir=false https://gitlab.com/predictive-cueing/tmpc-fork.git#predictive-cueing /home/mpmca-user/tmpc
RUN cd tmpc/ && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release -D TMPC_WITH_HPIPM=OFF -D TMPC_WITH_BLASFEO=ON -D TMPC_WITH_qpOASES=OFF -D TMPC_WITH_HPMPC=ON -D TMPC_WITH_CASADI_INTERFACE=ON ..  \
    && cmake --build . --target install --config Release -- -j4

FROM tmpc AS python-dev
# python-dev can be used to develop in mpmca/python, omsf, and casadi_extras simultaneously.
# Here, it is assumed their git repos will be checked out to /workspaces/mpmca-develop/*

COPY --from=matplotplusplus /usr/local/include/matplot /usr/local/include/matplot
COPY --from=matplotplusplus /usr/local/lib/Matplot++ /usr/local/lib/Matplot++
COPY --from=matplotplusplus /usr/local/lib/libmatplot* /usr/local/lib/
COPY --from=matplotplusplus /usr/local/lib/cmake/Matplot++ /usr/local/lib/cmake/Matplot++

ENV PYTHONPATH=/workspaces/mpmca-develop/casadi_extras/:/workspaces/mpmca-develop/mpmca/python/:/workspaces/mpmca-develop/omsf:/usr/local/python
ENV PYTHONIOENCODING=UTF-8
ENV PYTHONUNBUFFERED=1

CMD ["/bin/bash"]

FROM tmpc AS mpmca-dev
# mpmca-dev can be used to develop mpmca and simply has a copy of omsf and casadi_extras (different from python-dev)
ADD --keep-git-dir=false https://gitlab.com/predictive-cueing/casadi_extras.git#predictive-cueing /home/mpmca-user/casadi_extras
ADD --keep-git-dir=false https://gitlab.com/predictive-cueing/omsf-fork.git#v1.1 /home/mpmca-user/omsf

COPY --from=matplotplusplus /usr/local/include/matplot /usr/local/include/matplot
COPY --from=matplotplusplus /usr/local/lib/Matplot++ /usr/local/lib/Matplot++
COPY --from=matplotplusplus /usr/local/lib/libmatplot* /usr/local/lib/
COPY --from=matplotplusplus /usr/local/lib/cmake/Matplot++ /usr/local/lib/cmake/Matplot++

ADD https://www.python.org/ftp/python/3.12.3/Python-3.12.3.tar.xz /home/mpmca-user/python312/
RUN cd python312 && tar -Jxf Python-3.12.3.tar.xz && cd Python-3.12.3 && ./configure --enable-optimizations && make -j4 && make altinstall

ENV PYTHONPATH=/home/mpmca-user/omsf/:/home/mpmca-user/casadi_extras/:/usr/local/python

CMD ["/bin/bash"]
