# MPMCA Docker Containers

## Introduction

Docker container to facilitate building and using the MPMCA codebase in an containerized environment.

See [this](https://www.predictivecueing.com/docker) page for use instructions.

# List of dependencies

## Libeigen 

* https://eigen.tuxfamily.org/index.php?title=Main_Page
* https://gitlab.com/libeigen/eigen
* Version 3.3.7

## Googletest

* https://github.com/google/googletest
* Version 1.10.0

## Google Benchmark

* https://github.com/google/benchmark
* Version 1.5.0

## Blaze

* https://bitbucket.org/blaze-lib/blaze/src/master/
* https://bitbucket.org/blaze-lib/blaze.git
* Version 3.6

## nlohmann json

* https://github.com/nlohmann/json.git
* Version 3.7.3

## CasADi

* https://web.casadi.org/
* https://github.com/casadi/casadi.git
* Version 3.4.3

## Blasfeo

* https://github.com/giaf/blasfeo.git
* Commit 9c9e07923de801eab59937316888d20f5caa4f48

## HPMPC

* https://github.com/giaf/hpmpc.git
* Commit 35f36c98b7707aad7fbcb105d1a79fa5e6a0bd4c

## TMPC

* Forked from https://gitlab.syscop.de/mikhail.katliar/tmpc
* https://gitlab.com/predictive-cueing/tmpc-fork
* "predictive-cueing" branch.

## CasADi extras

* https://github.com/mkatliar/casadi_extras
* https://github.com/mkatliar/casadi_extras.git

## OMSF

* Forked from https://github.com/mkatliar/omsf
* https://gitlab.com/predictive-cueing/omsf-fork.git
* "predictive-cueing" branch.
